﻿                                            /** MODULO I - UNIDAD II - PRACTICA 4 **/
USE practica4;

/* 1. Averigua el DNI de todos los clientes */
SELECT
  c.dni
FROM
  cliente c
;

/* 2. Consulta todos los datos de todos los programas */
SELECT
  *
FROM
  programa p
;

/* 3. Obtén un listado con los nombres de todos los programas */
SELECT
  p.nombre, p.version
FROM
  programa p
;

/* 4. Genera una lista con todos los comercios */
SELECT
  *
FROM
  comercio c
;

/*
  5. Genera una lista de las ciudades con establecimientos donde se venden programas,
     sin que aparezcan valores duplicados (utiliza DISTINCT)
*/
SELECT
  DISTINCT c.ciudad
FROM
  comercio c
;

/* 6. Obtén una lista con los nombres de programas, sin que aparezcan valores duplicados (utiliza DISTINCT) */
SELECT
  DISTINCT p.nombre
FROM
  programa p
;

/* 7. Obtén el DNI más 4 de todos los clientes */
SELECT
  c.dni + 4
FROM
  cliente c
;

/* 8. Haz un listado con los códigos de los programas multiplicados por 7 */
SELECT
  p.codigo * 7
FROM
  programa p
;

/* 9. ¿Cuáles son los programas cuyo código es inferior o igual a 10? */
SELECT
  *
FROM
  programa p
WHERE
  p.codigo <= 10
;

/* 10. ¿Cuál es el programa cuyo código es 11? */
SELECT
  *
FROM
  programa p
WHERE
  p.codigo = 11
;

/* 11. ¿Qué fabricantes son de Estados Unidos? */
SELECT
  f.nombre
FROM
  fabricante f
WHERE
  f.pais = 'Estados Unidos'
;

/* 12. ¿Cuáles son los fabricantes no españoles? Utilizar el operador IN */
SELECT
  *
FROM
  fabricante f
WHERE
  pais NOT IN ('España')
;

/* 13. Obtén un listado con los códigos de las distintas versiones de Windows */
SELECT
  p.codigo
FROM
  programa p
WHERE
  p.nombre = 'Windows'
;

/* 14. ¿En qué ciudades comercializa programas El Corte Inglés? */
SELECT
  c.ciudad
FROM
  comercio c
WHERE
  c.nombre = 'El Corte Inglés'
;

/* 15. ¿Qué otros comercios hay, además de El Corte Inglés? Utilizar el operador IN */
SELECT
  c.nombre
FROM
  comercio c
WHERE
  c.nombre NOT IN ('El Corte Inglés')
;

/* 16. Genera una lista con los códigos de las distintas versiones de Windows y Access. Utilizar el operador IN */
SELECT
  p.codigo
FROM
  programa p
WHERE
  p.nombre IN ('Windows', 'Access')
;

/*
  17. Obtén un listado que incluya los nombres de los clientes de edades comprendidas entre 10 y 25 y de los mayores de 50 años.
   Da una solución con BETWEEN y otra sin BETWEEN 
*/
-- con between
SELECT
  c.nombre
FROM
  cliente c
WHERE
  c.edad BETWEEN 10 AND 25
  OR edad > 50
;

-- sin between
SELECT
  c.nombre
FROM
  cliente c
WHERE
  (c.edad > 10 AND edad < 25)
  OR edad > 50
;

/* 18. Saca un listado con los comercios de Sevilla y Madrid. No se admiten valores duplicados */
SELECT
  DISTINCT c.nombre
FROM
  comercio c
WHERE
  c.ciudad = 'Sevilla'
  OR c.ciudad = 'Madrid'
;

/* 19. ¿Qué clientes terminan su nombre en la letra “o”? */
SELECT
  *
FROM
  cliente c
WHERE
  c.nombre LIKE '%o'
;

/* 20. ¿Qué clientes terminan su nombre en la letra “o” y, además, son mayores de 30 años? */
SELECT
  *
FROM
  cliente c
WHERE
  c.nombre LIKE '%o'
  AND c.edad > 30
;

/*
  21. Obtén un listado en el que aparezcan los programas cuya versión finalice por una letra i, o cuyo nombre comience por una A
  o por una W 
*/
SELECT
  *
FROM
  programa p
WHERE
  version LIKE '%i'
  OR p.nombre LIKE 'A%'
  OR p.nombre LIKE 'W%'
;

/* 
  22.  Obtén un listado en el que aparezcan los programas cuya versión finalice por una letra i, o cuyo nombre comience por una A
  y termine por una S
*/
SELECT
  *
FROM
  programa p
WHERE
  version LIKE '%i'
  OR p.nombre LIKE 'A%'
  OR p.nombre LIKE '%S'
;

/* 23. Obtén un listado en el que aparezcan los programas cuya versión finalice por una letra i, y cuyo nombre no comience por una A */
SELECT
  *
FROM
  programa p
WHERE
  version LIKE '%i'
  AND p.nombre NOT LIKE 'A%'
;

/* 24. Obtén una lista de empresas por orden alfabético ascendente */
SELECT
  DISTINCT nombre 
FROM
  comercio
    ORDER BY nombre ASC
;

/* 25. Genera un listado de empresas por orden alfabético descendente */
SELECT
  DISTINCT nombre 
FROM
  comercio
    ORDER BY nombre DESC
;

/* 26. Obtén un listado de programas por orden de versión */
SELECT
  p.nombre,
  p.version
FROM
  programa p
    ORDER BY p.version
;

/* 27. Genera un listado de los programas que desarrolla Oracle */
SELECT
  p.nombre,
  p.version 
FROM programa p 
JOIN (
  SELECT
    d.codigo 
  FROM desarrolla d 
  JOIN (
    SELECT
      f.id_fab 
    FROM
      fabricante f 
    WHERE
       f.nombre = 'Oracle'
      ) c1 USING(id_fab)
) c2 USING(codigo)
;

/* 28. ¿Qué comercios distribuyen Windows? */
SELECT
  DISTINCT c.nombre 
FROM
  comercio c 
JOIN
(
  SELECT
    DISTINCT cif 
  FROM
    distribuye d
  JOIN
  (
    SELECT
      p.codigo 
    FROM
      programa p
  ) c1 USING(codigo)
) c2 USING(cif)
;

/* 29. Genera un listado de los programas y cantidades que se han distribuido a El Corte Inglés de Madrid */
SELECT
  p.nombre,
  p.version,
  c2.cantidad 
FROM
  programa p 
JOIN
(
  SELECT
    d.codigo,
    d.cantidad 
  FROM
    distribuye d 
  JOIN
  (
    SELECT
      c.cif 
    FROM
      comercio c 
    WHERE
      c.nombre = 'El Corte Inglés' 
      AND c.ciudad = 'Madrid'
  ) c1 USING(cif)
) c2 USING(codigo)
;

/* 30. ¿Qué fabricante ha desarrollado Freddy Hardest? */
SELECT
  f.nombre 
FROM
  fabricante f JOIN
  (
  SELECT
    d.id_fab 
  FROM
    desarrolla d 
  JOIN
  (
    SELECT
      p.codigo 
    FROM
      programa p 
    WHERE
      p.nombre = 'Freddy Hardest'
  )c1 USING(codigo)
)c2 USING(id_fab)
;

/* 31. Selecciona el nombre de los programas que se registran por Internet */
SELECT
  p.nombre 
FROM
  programa p 
JOIN
(
  SELECT
    r.codigo 
  FROM
    registra r 
  WHERE
    medio = 'Internet'
) c1 USING(codigo)
;

/* 32. Selecciona el nombre de las personas que se registran por Internet */
SELECT
  c.nombre
FROM
  cliente c 
JOIN
(
  SELECT
    dni
  FROM
    registra r 
  WHERE
    medio = 'Internet'
) c1 USING(dni)
;

/* 33. ¿Qué medios ha utilizado para registrarse Pepe Pérez? */
SELECT
  medio 
FROM
  registra r 
JOIN
(
  SELECT
    c.dni
  FROM
    cliente c
  WHERE
    c.nombre = 'Pepe Pérez'
) c1 USING(dni)
;

/* 34. ¿Qué usuarios han optado por Internet como medio de registro? */
SELECT
  c.nombre 
FROM
  cliente c 
JOIN
(
  SELECT
    dni 
  FROM 
    registra r 
  WHERE
    r.medio = 'Internet'
) c1 USING(dni)
;

/* 35. ¿Qué programas han recibido registros por tarjeta postal? */
SELECT
  p.nombre,
  p.version 
FROM
  programa p
JOIN
(
  SELECT
    r.codigo 
  FROM
    registra r 
  WHERE
    r.medio = 'Tarjeta Postal'
) c1 USING(codigo)
;

/* 36. ¿En qué localidades se han vendido productos que se han registrado por Internet? */
SELECT
  c.ciudad 
FROM
  comercio c 
JOIN
(
  SELECT
    r.cif 
  FROM
    registra r 
  WHERE
    r.medio = 'Internet'
) c1 USING(cif)
;

/*
  37. Obtén un listado de los nombres de las personas que se han registrado por Internet, junto al nombre de los programas para
  los que ha efectuado el registro
*/
SELECT
  c.nombre persona,
  p.nombre programa,
  p.version 
FROM
(
  SELECT
    r.dni, r.codigo 
  FROM
    registra r 
  WHERE
    r.medio = 'Internet'
) c1
JOIN cliente c USING(dni)
JOIN programa p USING(codigo)
;

/*
  38. Genera un listado en el que aparezca cada cliente junto al programa que ha registrado, el medio con el que lo ha hecho y el
  comercio en el que lo ha adquirido
*/
SELECT
  c.nombre persona, 
  p.nombre programa, 
  p.version, 
  r.medio, 
  c1.nombre comercio 
FROM
  registra r 
JOIN cliente c USING(dni)
JOIN comercio c1 USING(cif)
JOIN programa p USING(codigo)
;

/* 39. Genera un listado con las ciudades en las que se pueden obtener los productos de Oracle */
SELECT c.ciudad FROM comercio c JOIN
(
  SELECT DISTINCT d.cif FROM distribuye d JOIN
  (
    SELECT d.codigo FROM desarrolla d JOIN
    (
      SELECT f.id_fab FROM fabricante f WHERE f.nombre = 'Oracle'
    ) c1 USING(id_fab)
  ) c2 USING(codigo)
) c3 USING(cif)
;

/* 40. Obtén el nombre de los usuarios que han registrado Access XP */
SELECT c.nombre FROM cliente c JOIN
(
  SELECT r.dni FROM registra r JOIN
  (
    SELECT p.codigo FROM programa p WHERE p.nombre = 'Access' AND p.version = 'XP'
  ) C1 USING(codigo)
) C2 USING(dni)
;

/* 41. Nombre de aquellos fabricantes cuyo país es el mismo que ʻOracleʼ. (Subconsulta) */
SELECT f.nombre FROM fabricante f JOIN 
(
  SELECT f.pais FROM fabricante f WHERE f.nombre = 'Oracle'
) c1 USING(pais)
;

/* 42. Nombre de aquellos clientes que tienen la misma edad que Pepe Pérez. (Subconsulta) */
SELECT
  c.nombre 
FROM
  cliente c 
JOIN
(
  SELECT edad FROM cliente c WHERE c.nombre = 'Pepe Pérez'
) c1 USING(edad)
WHERE
  c.nombre <> 'Pepe Pérez'
;

/* 43. Genera un listado con los comercios que tienen su sede en la misma ciudad que tiene el comercio ʻFNACʼ (Subconsulta) */
SELECT c.nombre FROM comercio c
JOIN
(
  SELECT c.ciudad FROM comercio c WHERE c.nombre = 'FNAC'
) c1 USING(ciudad) WHERE c.nombre <> 'FNAC'
;

/* 44. Nombre de aquellos clientes que han registrado un producto de la misma forma que el cliente ʻPepe Pérezʼ (Subconsulta) */
SELECT
  c.nombre 
FROM
  cliente c 
JOIN
(
  SELECT DISTINCT r.dni FROM registra r JOIN
  (
   SELECT r.medio FROM registra r JOIN
    (
      SELECT c.dni FROM cliente c WHERE c.nombre = 'Pepe Pérez'
    ) c1 USING(dni)
  ) c2 USING(medio)
) c3 USING(dni)
WHERE
  c.nombre <> 'Pepe Pérez'
;

/* 45. Obtener el número de programas que hay en la tabla programas */
SELECT
 COUNT(*) nprogramas 
FROM
 programa p
;

/* 46. Calcula el número de clientes cuya edad es mayor de 40 años */
SELECT
  COUNT(*) nMayores40
FROM
  cliente c
WHERE
  c.edad > 40
;

/* 47. Calcula el número de productos que ha vendido el establecimiento cuyo CIF es 1 */
  -- con COUNT productos distintos vendidos
SELECT
  COUNT(*) nProductos
FROM
  distribuye d
WHERE
  d.cif = 1
;

  -- con SUM total de todos los productos vendidos
SELECT
  SUM(d.cantidad) Total
FROM
  distribuye d
WHERE
  d.cif = 1
;

/* 48. Calcula la media de programas que se venden cuyo código es 7 */
SELECT
  AVG(d.cantidad)
FROM
  distribuye d
WHERE
  d.codigo = 7
;

/* 49. Calcula la mínima cantidad de programas de código 7 que se ha vendido */
SELECT
  MIN(d.cantidad) minima
FROM
  distribuye d
WHERE
  d.codigo = 7
;

/* 50. Calcula la máxima cantidad de programas de código 7 que se ha vendido */
SELECT
  MAX(d.cantidad)
FROM
  distribuye d
WHERE
  d.codigo = 7
;

/* 51. ¿En cuántos establecimientos se vende el programa cuyo código es 7? */
SELECT
  COUNT(d.cif) nEstablecimientos
FROM
  distribuye d
WHERE
  d.codigo = 7
;

/* 52. Calcular el número de registros que se han realizado por Internet */
SELECT
  COUNT(*) nRegistros
FROM
  registra r
WHERE
  r.medio = 'Internet'
;

/* 53. Obtener el número total de programas que se han vendido en ʻSevillaʼ */
SELECT
  COUNT(d.codigo) nProgramas
FROM
  distribuye d
JOIN
  (SELECT
      c.cif
   FROM
    comercio c
   WHERE
    c.ciudad = 'Sevilla'
  ) c1
USING(cif)
;

/* 54. Calcular el número total de programas que han desarrollado los fabricantes cuyo país es ʻEstados Unidosʼ */
SELECT
  COUNT(d.codigo)
FROM
  desarrolla d
JOIN 
  (SELECT
    f.id_fab
   FROM
    fabricante f
   WHERE
    f.pais = 'Estados Unidos'
  ) c1
USING (id_fab)
;

/* 
  55. Visualiza el nombre de todos los clientes en mayúscula. En el resultado de la consulta debe aparecer también la longitud de
  la cadena nombre
*/
SELECT
  UPPER(c.nombre) MAYUSC,
  CHAR_LENGTH(c.nombre) longitud
FROM
  cliente c
;

/* 56. Con una consulta concatena los campos nombre y versión de la tabla PROGRAMA */
SELECT
  CONCAT(p.nombre, ' ', p.version)
FROM
  programa p
;